//
//  CDM.swift
//  UITableOMG
//
//  Created by Nikita Kolmykov on 13.03.2024.
//

import UIKit

/**
 *  Collection Display Manager.
 *  CDM-класс, реализующий протокол CDM обязан реализовать протоколы UICollectionViewDataSource и UICollectionViewDelegate.
 *  Является источником данных и делегатом для коллекции.
 */
public protocol CDM {
    
    associatedtype Delegate: TDMDelegate
    
    /// Делегат CDM-класса.
    var delegate: Delegate? { get }
    
    /// Массив моделей данных, которые будет отображать коллексия.
    var items: [Delegate.ItemType] { get set }
    
    /**
     Обновляет массив моделей данных.
     Реализация по умолчанию вызывает метод делегат `itemsUpdated()`
     
     - parameter items: Массив моделей данных.
     */

    mutating func updateItems(_ items: [Delegate.ItemType])
    
    /**
     Возвращает экземпляр источника данных для колекции.
     Реализация по умолчанию возвращает экземпляр CDM-класса (`self`).
     
     - parameter collectionView: Экцепляр коллекции, нуждающейся в источнике данных.
     
     - returns: Экземпляр источника данных.
     */
    func dataSource(forCollectionView collectionView: UICollectionView) -> UICollectionViewDataSource
    
    /**
     Возвращает экземпляр делегата для коллекции.
     Реализация по умолчанию возвращает экземпляр CDM-класса (`self`).
     
     - parameter collectionView: Экцепляр коллекции, нуждающейся в делегате.
     
     - returns: Экземпляр делегата.
     */
    func delegate(forCollectionView collectionView: UICollectionView) -> UICollectionViewDelegate
    
}

public extension CDM where Self: UICollectionViewDelegate & UICollectionViewDataSource {
    
    mutating func updateItems(_ items: [Delegate.ItemType]) {
        self.items = items
        delegate?.itemsUpdated()
    }
    
    func dataSource(forCollectionView collectionView: UICollectionView) -> UICollectionViewDataSource {
        return self
    }
    
    func delegate(forCollectionView collectionView: UICollectionView) -> UICollectionViewDelegate {
        return self
    }
    
}
