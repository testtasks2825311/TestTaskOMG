//
//  TDMDelegate.swift
//  UITableOMG
//
//  Created by Nikita Kolmykov on 12.03.2024.
//

import Foundation

public protocol TDMDelegate: AnyObject {
    
    associatedtype ItemType
    
    /// Вызывается после обновления массива моделей данных TDM/CDM-класса. Неоходимо обновить таблицу/коллекцию.
    func itemsUpdated()
    
    /**
     Вызывается при выборе ячейки таблицы/коллекции.
     
     - parameter item: Модель данных соответсвующая выбранной ячейке.
     */
    func itemSelected(_ item: ItemType)
    
}
