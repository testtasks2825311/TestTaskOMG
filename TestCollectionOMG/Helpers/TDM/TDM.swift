//
//  TDM.swift
//  UITableOMG
//
//  Created by Nikita Kolmykov on 12.03.2024.
//

import UIKit

/**
 *  Table Display Manager.
 *  TDM-класс, реализующий протокол TDM обязан реализовать протоколы UITableViewDataSource и UITableViewDelegate.
 *  Является источником данных и делегатом для таблицы.
 */
public protocol TDM {

    associatedtype Delegate: TDMDelegate

    /// Делегат TDM-класса.
    var delegate: Delegate? { get }

    /// Массив моделей данных, которые будет отображать таблица.
    var items: [Delegate.ItemType] { get set }

    /**
     Обновляет массив моделей данных.
     Реализация по умолчанию вызывает метод делегат `itemsUpdated()`

     - parameter items: Массив моделей данных.
     */
    mutating func updateItems(_ items: [Delegate.ItemType])

    /**
     Возвращает экземпляр источника данных для таблицы.
     Реализация по умолчанию возвращает экземпляр TDM-класса (`self`).

     - parameter tableView: Экцепляр таблицы, нуждающейся в источнике данных.

     - returns: Экземпляр источника данных.
     */
    func dataSource(forTableView tableView: UITableView) -> UITableViewDataSource

    /**
     Возвращает экземпляр делегата для таблицы.
     Реализация по умолчанию возвращает экземпляр TDM-класса (`self`).

     - parameter tableView: Экцепляр таблицы, нуждающейся в делегате.

     - returns: Экземпляр делегата.
     */
    func delegate(forTableView tableView: UITableView) -> UITableViewDelegate

}

public extension TDM where Self: UITableViewDelegate & UITableViewDataSource {

    mutating func updateItems(_ items: [Delegate.ItemType]) {
        self.items = items
        delegate?.itemsUpdated()
    }
    
}
