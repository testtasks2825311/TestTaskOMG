//
//  MockDataManager.swift
//  UITableOMG
//
//  Created by Nikita Kolmykov on 13.03.2024.
//

import Foundation

final class MockDataManager {
    
    /// Создание мок моделей для ячеек
    /// - Parameters:
    ///   - modelCount: Количество моделей которое хотим получить
    ///   - numberCount: Количество чисел в горизонтальном ряду
    public func createCollectionRectanglesCellModels(_ modelsCount: Int,
                                                 numbersCount: Int) -> [CollectionRectanglesCellModel] {
        
        var arrayModel: [CollectionRectanglesCellModel] = []
        for _ in 0..<modelsCount {
            var arrayNumbers: [Int] = []
            for _ in 0..<numbersCount {
                arrayNumbers.append(Int.random(in: 1...99))
            }
            let model = CollectionRectanglesCellModel(numbers: arrayNumbers, offsetScroll: 0)
            arrayModel.append(model)
        }
        return arrayModel
    }
}
