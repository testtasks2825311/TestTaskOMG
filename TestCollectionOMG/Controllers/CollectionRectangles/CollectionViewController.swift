//
//  CollectionViewController.swift
//  UITableOMG
//
//  Created by Nikita Kolmykov on 13.03.2024.
//

import UIKit

final class CollectionViewController: UIViewController {
    
    // MARK: - Properties
    
    lazy var collectionView = UICollectionView(frame: .zero, collectionViewLayout: createCollectionViewLayout())
        .set(\.showsVerticalScrollIndicator, to: false)
    
    private var cdm = CollectionRectanglesCDM<CollectionViewController>()
    
    private var dataManager = MockDataManager()
    
    // MARK: - Lifecycle
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        layoutViews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setItems()
    }
    
    // MARK: - Methods
    
    private func setItems() {
        let models = dataManager.createCollectionRectanglesCellModels(200, numbersCount: 15)
        cdm.updateItems(models)
    }
    
    // MARK: - Layout
    
    private func layoutViews() {
        layoutCollectionView()
    }
    
    private func layoutCollectionView() {
        view.addSubview(collectionView)
        collectionView.layout {
            collectionView.topAnchor.constraint(equalTo: view.topAnchor)
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor)
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        }
        
        collectionView.register(CollectionRectangleCell.self, forCellWithReuseIdentifier: "\(CollectionRectangleCell.self)")
        collectionView.delegate = cdm.delegate(forCollectionView: collectionView)
        collectionView.dataSource = cdm.dataSource(forCollectionView: collectionView)
        cdm.delegate = self
        itemsUpdated()
    }
    
}

extension CollectionViewController: CollectionRectanglesCDMDelegate {
    
    typealias ItemType = CollectionRectanglesCellModel

    func itemsUpdated() {
        collectionView.reloadData()
    }
    
    func itemSelected(_ item: CollectionRectanglesCellModel) {
        
    }
    
}

// MARK: - CompositionLayout

extension CollectionViewController {
    
    private func createCollectionViewLayout() -> UICollectionViewCompositionalLayout {
        return UICollectionViewCompositionalLayout { (section, _) -> NSCollectionLayoutSection? in
            let item = NSCollectionLayoutItem(
                layoutSize: NSCollectionLayoutSize(
                    widthDimension: .fractionalWidth(1),
                    heightDimension: .fractionalHeight(1)
                )
            )
            
            // group
            let group = NSCollectionLayoutGroup.horizontal(
                layoutSize: NSCollectionLayoutSize(
                    widthDimension: .fractionalWidth(1),
                    heightDimension: .absolute(80)
                ),
                subitem: item,
                count: 1
            )
            
            // section
            let section = NSCollectionLayoutSection(group: group)
            section.orthogonalScrollingBehavior = .continuous
            section.contentInsets = NSDirectionalEdgeInsets(top: 4, leading: 0, bottom: 4, trailing: 0)
            
            // return
            return section
        }
    }
}
