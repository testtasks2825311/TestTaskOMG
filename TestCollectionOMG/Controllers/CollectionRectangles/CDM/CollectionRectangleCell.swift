//
//  CollectionRectangleCell.swift
//  UITableOMG
//
//  Created by Nikita Kolmykov on 13.03.2024.
//

import UIKit

final class CollectionRectangleCell: UICollectionViewCell {
    
    // MARK: - Properties
    
    private let scrollView = UIScrollView()
        .set(\.showsHorizontalScrollIndicator, to: false)
    
    private let hStack = UIStackView()
        .set(\.axis, to: .horizontal)
        .set(\.spacing, to: 8)
        .set(\.alignment, to: .center)
    
    private var rectangles: [RectangleWithNumber] = []
    
    private var timer = Timer()
    
    public var model: CollectionRectanglesCellModel?
    
    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        layoutCell()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        model = nil
        scrollView.contentOffset = .zero
    }
    
    // MARK: - Methods
    
    private func setupRectangles() {
        for index in 0..<(model?.numbers.count ?? 0) {
            rectangles.append(RectangleWithNumber())
            hStack.addArrangedSubview(rectangles[index])
        }
    }
    
    private func changeNumberRandomNumber() {
        if let model = model {
            let randomIndex = Int.random(in: 0..<model.numbers.count)
            let newNumber = Int.random(in: 1...99)
            self.model?.numbers[randomIndex] = newNumber
            rectangles[randomIndex].changeNumber(newNumber)
        }
    }
    
    public func startChangeRandomNumber() {
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { [weak self] _ in
            self?.changeNumberRandomNumber()
        }
    }
    
    public func endChangeRandomNumber() {
        timer.invalidate()
    }
    
    public func setModel(_ model: CollectionRectanglesCellModel) {
        self.model = model
        setupRectangles()
        scrollView.contentOffset.x = model.offsetScroll
    }
    
    // MARK: - Layout
    
    private func layoutCell() {
        self.heightAnchor.constraint(equalToConstant: 80).isActive = true
        layoutScrollView()
        layoutHStack()
    }
    
    private func layoutScrollView() {
        self.addSubviews(scrollView)
        scrollView.layout {
            scrollView.topAnchor.constraint(equalTo: self.topAnchor)
            scrollView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
            scrollView.leadingAnchor.constraint(equalTo: self.leadingAnchor)
            scrollView.trailingAnchor.constraint(equalTo: self.trailingAnchor)
        }
        
        scrollView.delegate = self
    }
    
    private func layoutHStack() {
        scrollView.addSubview(hStack)
        hStack.layout {
            hStack.topAnchor.constraint(equalTo: scrollView.topAnchor)
            hStack.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor)
            hStack.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor)
            hStack.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor)
        }
    }
}

// MARK: - ScrollDelegate

extension CollectionRectangleCell: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        model?.offsetScroll = scrollView.contentOffset.x
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        model?.offsetScroll = scrollView.contentOffset.x
    }
}
