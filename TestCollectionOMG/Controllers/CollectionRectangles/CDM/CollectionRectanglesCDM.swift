//
//  CollectionRectanglesCDM.swift
//  UITableOMG
//
//  Created by Nikita Kolmykov on 13.03.2024.
//

import UIKit

protocol CollectionRectanglesCDMDelegate: TDMDelegate {}

/// Оперирем секциями
final class CollectionRectanglesCDM<Delegate: CollectionRectanglesCDMDelegate>: NSObject,
                                                                                CDM,
                                                                                UICollectionViewDataSource,
                                                                                UICollectionViewDelegateFlowLayout where Delegate.ItemType == CollectionRectanglesCellModel {
    var delegate: Delegate?
    
    var items: [Delegate.ItemType] = []
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(CollectionRectangleCell.self)",
                                                      for: indexPath) as! CollectionRectangleCell
        
        let model = items[indexPath.section]
        cell.setModel(model)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? CollectionRectangleCell {
            cell.startChangeRandomNumber()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? CollectionRectangleCell {
            cell.endChangeRandomNumber()
            if let model = cell.model {
                items[indexPath.section] = model
            }
        }
    }
}
