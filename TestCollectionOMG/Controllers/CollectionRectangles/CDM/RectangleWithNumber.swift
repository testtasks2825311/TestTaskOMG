//
//  RectancleWithNumber.swift
//  UITableOMG
//
//  Created by Nikita Kolmykov on 12.03.2024.
//

import UIKit

final class RectangleWithNumber: UIButton {
    
    // MARK: - Properties
    
    private let numberLabel = UILabel()
        .set(\.font, to: .boldSystemFont(ofSize: 18))
        .set(\.textColor, to: .black)
        .set(\.text, to: "\(Int.random(in: 1...99))")
    
    // MARK: - Lifecycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
     
        layoutViews()
        
        /// Таргет на анимацию
        addTarget(self, action: #selector(animateDown), for: [.touchDown, .touchDragEnter])
        addTarget(self, action: #selector(animateUp), for: [.touchDragExit, .touchCancel, .touchUpInside, .touchUpOutside])
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        layoutViews()
    }
    
    // MARK: - Methods
    
    public func changeNumber(_ number: Int) {
        UIView.transition(with: numberLabel,
                          duration: 0.2,
                          options: .transitionCrossDissolve,
                          animations: { [weak self] in
            self?.numberLabel.text = "\(number)"
        }, completion: nil)
    }
    
    
    @objc private func animateDown(sender: UIButton) {
        animate(sender, transform: CGAffineTransform.identity.scaledBy(x: 0.8, y: 0.8))
    }
    
    @objc private func animateUp(sender: UIButton) {
        animate(sender, transform: .identity)
    }
    
    private func animate(_ button: UIButton, transform: CGAffineTransform) {
        UIView.animate(withDuration: 0.15,
                       delay: 0,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 3,
                       options: [.curveEaseInOut],
                       animations: {
            button.transform = transform
        }, completion: nil)
    }
    
    // MARK: - Layout
    
    private func layoutViews() {
        
        self.layout {
            self.heightAnchor.constraint(equalToConstant: 80)
            self.widthAnchor.constraint(equalToConstant: 80)
        }
        
        self.layer.cornerRadius = 16
        self.layer.borderWidth = 4
        self.layer.borderColor = UIColor(.gray).cgColor
        self.backgroundColor = .purple
        
        layoutNumberLabel()
    }
    
    private func layoutNumberLabel() {
        self.addSubviews(numberLabel)
        numberLabel.layout {
            numberLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor)
            numberLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor)
        }
    }
}
