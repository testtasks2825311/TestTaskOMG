//
//  CollectionRectanglesCellModel.swift
//  UITableOMG
//
//  Created by Nikita Kolmykov on 13.03.2024.
//

import Foundation


struct CollectionRectanglesCellModel {
    /// Цифры которые отображаются в квадратах
    var numbers: [Int]
    /// Для сохранения офсет скролла
    var offsetScroll: CGFloat
}
